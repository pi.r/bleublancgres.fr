#!/usr/bin/env bash
set -euo pipefail

mogrify -resize 1250x1250 -path img/ newimages/*
mogrify -resize 350x350 -path img/thumbs/ newimages/*
